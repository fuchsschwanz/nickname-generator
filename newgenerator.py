import random
import itertools

consonants = [
    ('m', 2.8), ('d', 3.25), ('n', 5.52),
    ('r', 4.69), ('z', 5.64), ('l', 2.1),
    ('j', 2.28), ('g', 1.42), ('w', 4.65),
    ('p', 3.13), ('f', 0.3), ('t', 3.98),
    ('s', 4.32), ('k', 3.51), ('x', 0.02)
    ]

vowels = [
    ('a', 8.91), ('i', 8.21), ('o', 7.75),
    ('u', 2.5), ('e', 7.66), ('y', 3.76)
    ]

letters = consonants + vowels


def choose(thing):
    g = 0
    totals = []
    for i in list(zip(*thing))[1]:
        g += i
        totals.append(g)

    rnd = random.random() * g
    for i, total in enumerate(totals):
        if rnd < total:
            x = thing[i][0]
            return x
            break  

def makeword():
    counter = 0
    word = choose(letters)
    consamount = random.randint(1, 3)
    wordlen = random.randint(3, 8)
    
    vowelslist = list(zip(*vowels))[0]
    conslist = list(zip(*consonants))[0]

    for x in range(wordlen):
        if counter == consamount:
            word += choose(vowels)
            counter = 0
            consamount = random.randint(1, 3)
        elif word[x] in vowelslist:
            word += choose(consonants)
            counter += 1
        elif word[x] in conslist:
            word += choose(letters)
    print(word)
    return word
