
# Nickname generator

Maybe I should name it "hit the keyboard with your head simulator"? 

Despite that, it gave me some good name ideas, and some good names as well.


The idea is simple, I took numbers from there: [https://sjp.pwn.pl/poradnia/haslo/frekwencja-liter-w-polskich-tekstach;7072.html](https://sjp.pwn.pl/poradnia/haslo/frekwencja-liter-w-polskich-tekstach;7072.html), 
choose random word lenght (3-8), random consonants amount before vowel (1-3), and that's it.

Usage: 

Enter - generate new word  
s - save last generated word to a file  
x - exit  