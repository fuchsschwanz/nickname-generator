import random
import itertools
import newgenerator


def main():

    ostatnie = ""

    print("enter - generate word\ns - save last word\nx - exit\n\n")

    while True:
        i = input()
        file = open('file.txt', 'a')
        if i == "":
            print ("Random word: ")
            ostatnie = newgenerator.makeword() + "\n"
        elif i == "s":
            file.write(ostatnie)
            print("Saved!")
        elif i == "x":
            file.close()
            break
        else:
            continue


if __name__ == '__main__':
    main()
    